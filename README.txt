README for FISH. 

FILES IN THIS DISTRIBUTION:
1.  SAMPLES.txt -- tells you how to run and view examples. Sample data is provided.
2.  INSTALL.txt -- tells you how to compile and install FISH-1.0 on your own system.
3.  makefile -- for compiling the source code
4.  LICENSE.txt -- to tell you what you can and cannot do with this program.
5.  README.txt -- the one you are currently reading
 
DIRECTORIES IN THIS DISTRIBUTION:
1.  bin -- after compiling source code, this is where the "fish" executable is copied.
2.  docs -- contains the manual in postscript,html,and pdf formats.
3.  sample_input -- contains sample data and a sample control file.
4.  sample_ouput -- contains sample output
5.  src -- directory contain all source files.


THE MANUAL:
Please read the manual before using FISH-1.0.  It is located in "docs" and is
called "fishmanual.pdf".

