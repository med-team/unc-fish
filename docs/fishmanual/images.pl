# LaTeX2HTML 2002 (1.62)
# Associate images original text with physical files.


$key = q/h=mslashn;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="78" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img6.png"
 ALT="$ h=m/n$">|; 

$key = q/F;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img22.png"
 ALT="$ F$">|; 

$key = q/displaystyled_T=frac{1}{2}+sqrt{frac{{log}(1-T)}{{log}(1-h)}}+frac{1}{4};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="230" HEIGHT="80" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img11.png"
 ALT="$\displaystyle d_T = \frac{1}{2}+\sqrt{\frac{{\log}(1-T)}{{\log}(1-h)}}+\frac{1}{4}$">|; 

$key = q/>;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img20.png"
 ALT="$ &gt;$">|; 

$key = q/p;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img25.png"
 ALT="$ p$">|; 

$key = q/(X_j,Y_j);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="68" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img9.png"
 ALT="$ (X_j, Y_j)$">|; 

$key = q/N;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img21.png"
 ALT="$ N$">|; 

$key = q/p_u=h(nh)^{k-1};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="121" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img28.png"
 ALT="$ p_u = h(nh)^{k-1}$">|; 

$key = q/m;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img4.png"
 ALT="$ m$">|; 

$key = q/d=|X_i-X_j|+|Y_j-Y_i|;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="208" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img10.png"
 ALT="$ d = \vert X_i - X_j\vert + \vert Y_j - Y_i\vert$">|; 

$key = q/i;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img1.png"
 ALT="$ i$">|; 

$key = q/F(F-1)slash2;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="100" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img23.png"
 ALT="$ F(F-1)/2$">|; 

$key = q/d_c;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img14.png"
 ALT="$ d_c$">|; 

$key = q/displaystylew={cases}1&text{ifthepointisthefirstintheblock},0.25&text{ifaninversionisimpliedbytheextension},0.75&text{otherwise}.{cases};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="440" HEIGHT="101" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img16.png"
 ALT="$\displaystyle w = \begin{cases}1 &amp; \text{if the point is the first in the block...
... inversion is implied by the extension},\ 0.75 &amp; \text{otherwise}. \end{cases}$">|; 

$key = q/(X_i,Y_i);MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img8.png"
 ALT="$ (X_i, Y_i)$">|; 

$key = q/j;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img2.png"
 ALT="$ j$">|; 

$key = q/displaystylep=1-e^{-rcp_u};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="119" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img26.png"
 ALT="$\displaystyle p = 1-e^{-rc p_u}$">|; 

$key = q/displaystyler=wfrac{1slashd_c}{sum_{1}^{n}(1slashd_c)};MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="135" HEIGHT="62" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img13.png"
 ALT="$\displaystyle r = w \frac{1/d_c}{\sum_{1}^{n}(1/d_c)}$">|; 

$key = q/n;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img5.png"
 ALT="$ n$">|; 

$key = q/igeqj;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="45" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img3.png"
 ALT="$ i \geq j$">|; 

$key = q/w;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img15.png"
 ALT="$ w$">|; 

$key = q/d_T;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img18.png"
 ALT="$ d_T$">|; 

$key = q/L;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img24.png"
 ALT="$ L$">|; 

$key = q/r;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img12.png"
 ALT="$ r$">|; 

$key = q/backslash;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img30.png"
 ALT="$ \backslash$">|; 

$key = q/h;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img19.png"
 ALT="$ h$">|; 

$key = q/slash;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="5" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img29.png"
 ALT="$ \/$">|; 

$key = q/|(|X_i-X_j|)-(|Y_j-Y_i|)|;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="213" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img17.png"
 ALT="$ \vert(\vert X_i - X_j\vert)-(\vert Y_j - Y_i\vert)\vert$">|; 

$key = q/rc;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img27.png"
 ALT="$ rc$">|; 

$key = q/T;MSF=1.6;LFS=12;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img7.png"
 ALT="$ T$">|; 

1;

