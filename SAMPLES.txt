Samples...
Two directories exist in the FISH-1.0 package, "sample_input" and
"sample_output."  They are described below.

sample_input
------------
Inside the sample_input directory of the FISH distributions is a control
file(control.txt) and a data directory called "data" which contains all
the files mentioned in "control.txt."


sample_output
-------------
The following list consists of sample outputs created after running the
following command on the sample_input data:
"fish -b blocks.txt -B simpleblocks.txt -g grids.txt -C contigs.txt > fish_out.txt" 
     ** the ">" in the above sends the ouput to the file "fish_out.txt"

* blocks.txt  --  human readable output of the homologous blocks detected by
                  FISH.
* simpleblocks.txt  -- computer readable output of the homologous blocks
                       detected by FISH.
* grids.txt -- output of grid FISH constructed to find blocks.
* contigs.txt -- output of contigs FISH constructed to create grids.
* fishout.txt -- standard output of FISH.

