Source: unc-fish
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/med-team/unc-fish
Vcs-Git: https://salsa.debian.org/med-team/unc-fish.git
Homepage: http://labs.bio.unc.edu/Vision/FISH/

Package: unc-fish
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Fast Identification of Segmental Homology
 FISH is software for identifying regions of common ancestry between
 genome maps.  Fast identification and statistical evaluation of
 segmental homologies in comparative maps. 
 .
 Development and maintenance of FISH is supported by funding from the
 National Science Foundation (Plant Genome Research Program Grants
 DBI-0110069 and DBI-0227314 to TJV and DMS-0102008 to PPC).

Package: unc-fish-tests
Architecture: all
Priority: extra
Depends: ${misc:Depends}
Suggests: unc-fish
Enhances: unc-fish
Description: Tests for Fast Identification of Segmental Homology (fish)
 FISH is software for identifying regions of common ancestry between
 genome maps.  Fast identification and statistical evaluation of
 segmental homologies in comparative maps. 
 .
 Development and maintenance of FISH is supported by funding from the
 National Science Foundation (Plant Genome Research Program Grants
 DBI-0110069 and DBI-0227314 to TJV and DMS-0102008 to PPC).
 .
 This package contains the example data provided by upstream to enable
 the user to test fish.
